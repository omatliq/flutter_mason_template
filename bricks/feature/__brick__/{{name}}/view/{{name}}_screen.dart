import 'package:flutter/material.dart';

class {{#pascalCase}}{{name}}Screen{{/pascalCase}} extends StatelessWidget {
  const {{#pascalCase}}{{name}}Screen{{/pascalCase}}({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('{{name}}'),
      ),
    );
  }
}
