part of '{{name}}_controller.dart';

enum {{#pascalCase}}{{name}}Status{{/pascalCase}} {
  initial,
  loading,
  loadingSuccess,
  loadingFailure,
}

class {{#pascalCase}}{{name}}State{{/pascalCase}} extends Equatable {
  final {{#pascalCase}}{{name}}Status{{/pascalCase}} status;
  final String? message;

  const {{#pascalCase}}{{name}}State{{/pascalCase}}({
    this.status = {{#pascalCase}}{{name}}Status{{/pascalCase}}.initial,
    this.message,
  });

  {{#pascalCase}}{{name}}State{{/pascalCase}} copyWith({
    {{#pascalCase}}{{name}}Status{{/pascalCase}}? status,
    String? message,
  }) {
    return {{#pascalCase}}{{name}}State{{/pascalCase}}(
      status: status ?? this.status,
      message: message ?? this.message,
    );
  }

  @override
  List<Object?> get props => [];
}
