import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

part '{{name}}_state.dart';

final {{name}}ControllerProvider =
StateNotifierProvider<{{#pascalCase}}{{name}}Controller{{/pascalCase}}, {{#pascalCase}}{{name}}State{{/pascalCase}}>(
      (ref) => {{#pascalCase}}{{name}}Controller{{/pascalCase}}(),
);

class {{#pascalCase}}{{name}}Controller{{/pascalCase}} extends StateNotifier<{{#pascalCase}}{{name}}State{{/pascalCase}}>{
  {{#pascalCase}}{{name}}Controller{{/pascalCase}}()
    :  super({{#pascalCase}}{{name}}State{{/pascalCase}}());
}