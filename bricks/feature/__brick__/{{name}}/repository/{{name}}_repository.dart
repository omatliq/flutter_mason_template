import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../utils/utils.dart';

final {{#camelCase}}{{name}}{{/camelCase}}RepositoryProvider = Provider(
      (ref) => {{#pascalCase}}{{name}}Repository{{/pascalCase}}(
    client: ref.read(apiClientProvider),
  ),
);

class {{#pascalCase}}{{name}}Repository{{/pascalCase}} {
{{#pascalCase}}{{name}}Repository{{/pascalCase}}({
    required ApiClient client,
  }) : _client = client;
  final ApiClient _client;

}
