import 'package:flutter/material.dart';
import '/src/constants/constants.dart' as constants;
import '/src/routing/routing.dart';

class {{#pascalCase}}{{app_name}}App{{/pascalCase}} extends StatelessWidget {
  const {{#pascalCase}}{{app_name}}App{{/pascalCase}}({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: getAppTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: true,
      ),
      onGenerateRoute: Routing.onGenerateRoute,
    );
  }

  String get getAppTitle => '{{app_name}}'; //Your App Name
}
