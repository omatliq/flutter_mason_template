import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class Routing {
  static const String splashScreen = '/';

  static Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashScreen:
        // return MaterialPageRoute(
        //   builder: (context) => const SplashScreen(),
        //   settings: const RouteSettings(name: splashScreen),
        // );
      default:
        return null;
    }
  }

  static Future push(
    BuildContext context,
    String routeName, {
    dynamic arguments,
  }) {
    FocusScope.of(context).unfocus();
    return Navigator.pushNamed(
      context,
      routeName,
      arguments: arguments,
    );
  }

  static Future pushReplacement(BuildContext context, String routeName) {
    return Navigator.pushReplacementNamed(context, routeName);
  }

  static void pop(BuildContext context) {
    return Navigator.pop(context);
  }

  static Future showModalBottomSheet({
    required BuildContext context,
    required Widget screen,
  }) {
    return showCupertinoModalBottomSheet(
      context: context,
      expand: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return screen;
      },
    );
  }
}
