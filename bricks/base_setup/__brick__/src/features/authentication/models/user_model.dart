import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int id;

  const User({
    required this.id,
  });

  User copyWith({
    int? id,
  }) {
    return User(
      id: id ?? this.id,
    );
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
      };

  @override
  List<Object?> get props => [id];
}
