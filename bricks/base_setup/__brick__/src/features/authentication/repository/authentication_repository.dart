import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../utils/utils.dart';

final authenticationRepositoryProvider = Provider(
  (ref) => AuthenticationRepository(
    client: ref.read(apiClientProvider),
  ),
);

class AuthenticationRepository {
  AuthenticationRepository({
    required ApiClient client,
  }) : _client = client;
  final ApiClient _client;


}
