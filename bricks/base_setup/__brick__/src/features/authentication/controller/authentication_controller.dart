import 'dart:convert';
import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../utils/utils.dart';
import '../models/models.dart';
part 'authentication_state.dart';

final authControllerProvider =
    StateNotifierProvider<AuthenticationController, AuthenticationState>(
  (ref) => AuthenticationController(
    preferenceManager: ref.read(preferenceManagerProvider),
  )..checkLogin(),
);

class AuthenticationController extends StateNotifier<AuthenticationState> {
  AuthenticationController({
    required PreferenceManager preferenceManager,
  })  : _preferenceManager = preferenceManager,
        super(const AuthenticationState());

  final PreferenceManager _preferenceManager;

  bool get isSignedIn =>
      _preferenceManager.getString(PreferenceManager.accessTokenKey) != null;

  bool get isFirstTime =>
      _preferenceManager.getBool(PreferenceManager.isFirstTimeKey) ?? true;

  set isFirstTime(bool value) =>
      _preferenceManager.setBool(PreferenceManager.isFirstTimeKey, value);

  checkLogin() {
    final status = isSignedIn
        ? AuthenticationStatus.authenticated
        : AuthenticationStatus.unauthenticated;

    User? user;

    if (isSignedIn) {
      final String? userRaw = _preferenceManager.getString(
        PreferenceManager.userKey,
      );

      if (userRaw != null) {
        user = User.fromJson(jsonDecode(userRaw));
      }
    }

    _updateState(
      state.copyWith(
        status: status,
        user: user,
      ),
    );
  }

  authenticationStatusChanged(
    AuthenticationStatus status, {
    User? user,
  }) async {
    if (status == AuthenticationStatus.authenticated) {
      await _preferenceManager.setString(
        key: PreferenceManager.userKey,
        value: jsonEncode(
          user!.toJson(),
        ),
      );
    }
    _updateState(state.copyWith(status: status, user: user));
  }

  logout() async{
    await _preferenceManager.removeItem(PreferenceManager.accessTokenKey);
    await _preferenceManager.removeItem(PreferenceManager.userKey);

    _updateState(
      state.copyWith(
        status: AuthenticationStatus.unauthenticated,
        user: null,
      ),
    );
  }

  _updateState(AuthenticationState newState) {
    if (mounted) {
      state = newState;
    }
  }
}
