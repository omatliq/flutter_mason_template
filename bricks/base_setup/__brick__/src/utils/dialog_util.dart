import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DialogUtil {
  static Future showPlatformSpecificDialog({
    required BuildContext context,
    required String message,
    VoidCallback? onPressed,
    bool barrierDismissible = true,
  }) {
    if (Platform.isAndroid) {
      return showDialog(
        context: context,
        barrierDismissible: barrierDismissible,
        builder: (context) => WillPopScope(
          onWillPop: () async {
            if (barrierDismissible) {
              return true;
            }
            return false;
          },
          child: AlertDialog(
            content: Text(
              message,
            ),
            actions: [
              TextButton(
                onPressed: () {
                  onPressed?.call();
                  Navigator.pop(context, true);
                },
                child: const Text(
                  'Close',
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            content: Text(message),
            actions: <Widget>[
              CupertinoDialogAction(
                child: const Text('Close'),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              ),
            ],
          );
        },
      );
    }
  }
}
