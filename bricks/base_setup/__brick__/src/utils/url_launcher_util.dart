import 'package:new_app/src/utils/utils.dart';
import 'package:url_launcher/url_launcher_string.dart';

class UrlLauncherUtil {
  static void launchUrl(String url) async {
    final canLaunch = await canLaunchUrlString(url);

    if (canLaunch) {
      await launchUrlString(url);
    } else {
      ToastUtil.showToastMessage(message: 'Could not launch $url');
    }
  }
}
