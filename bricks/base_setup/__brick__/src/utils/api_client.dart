import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'preference_manager.dart';

class UnAuthorizedException implements Exception {
  final String message;

  UnAuthorizedException(this.message);
}

final httpClientProvider = Provider(
  (ref) => http.Client(),
);

final apiClientProvider = Provider(
  (ref) => ApiClient(
    client: ref.read(httpClientProvider),
    preferenceManager: ref.read(preferenceManagerProvider),
  ),
);

class ApiClient {
  final http.Client _client;
  final PreferenceManager _preferenceManager;

  const ApiClient({
    required http.Client client,
    required PreferenceManager preferenceManager,
  })  : _preferenceManager = preferenceManager,
        _client = client;

  Map<String, String> get _headers {
    final accessToken =
        _preferenceManager.getString(PreferenceManager.accessTokenKey);

    final headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };

    if (accessToken != null) {
      headers['Authorization'] = 'Bearer $accessToken';
    }
    return headers;
  }

  Future getRequest(String url) async {
    try {
      _print(url);
      _print(_headers);
      final response = await _client.get(
        Uri.parse(url),
        headers: _headers,
      );

      if (response.statusCode == 401) {
        throw UnAuthorizedException(
            response.reasonPhrase ?? 'Failed to authenticate.');
      }

      if (response.statusCode == 200) {
        _print(response.body);
        return jsonDecode(response.body);
      }
      throw Exception(response.reasonPhrase);
    } on SocketException catch (_) {
      throw 'No Internet Connection';
    } catch (e) {
      rethrow;
    }
  }

  Future<Map<String, dynamic>> postRequest({
    required String url,
    required Map<String, dynamic> body,
  }) async {
    try {
      _print(url);
      _print(body);
      _print(_headers);
      final response = await _client.post(
        Uri.parse(url),
        body: jsonEncode(body),
        headers: _headers,
      );
      if (response.statusCode == 401) {
        throw UnAuthorizedException(
            response.reasonPhrase ?? 'Failed to authenticate.');
      }

      if (response.statusCode == 200 ||
          response.statusCode == 201 ||
          response.statusCode == 400 ||
          response.statusCode == 403) {
        _print(jsonDecode(response.body));
        return jsonDecode(response.body);
      } else {
        _print(response.reasonPhrase);
        throw Exception(response.reasonPhrase);
      }
    } on SocketException catch (_) {
      throw 'No Internet Connection';
    }
  }

  _print(dynamic text) {
    if (kDebugMode) {
      debugPrint(text.toString());
    }
  }
}
