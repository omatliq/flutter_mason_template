class Validator {
  Validator._();

  static String? validateEmail(String? email) {
    if (email != null && email.isNotEmpty) {
      final isValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(email);

      if (isValid) {
        if (email.contains(' ')) {
          return 'Email must not contain whitespace.';
        }
        return null;
      } else {
        return 'Invalid email.';
      }
    }
    return 'Email is required.';
  }

  /// Checks if the given [value] is a valid email address.
  static String? validateEmailAddressOrUsername(String value) {
    if (value.isEmpty) {
      return 'Email or username is required.';
    }
    if (value.contains('@')) {
      return validateEmail(value);
    } else {
      return validateUsername(value);
    }
  }

  static String? validatePassword(String? password) {
    if (password != null && password.isNotEmpty) {
      final isValid = RegExp(
              r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#%^\$&*~]).{8,}$')
          .hasMatch(password);

      // final isValid = password.length >= 8;

      if (isValid) {
        if (password.startsWith(' ') || password.endsWith(' ')) {
          return 'Password must not contain whitespace.';
        }
        return null;
      } else {
        // return 'Password must be 8 characters long.';
        return 'Password must be at least 8 characters long and should contain uppercase and lowercase letters, special characters(!,@,#,\$,%,^,&,*) and numbers.';
      }
    }

    return 'Password is required.';
  }

  static String? validateUsername(String value) {
    if (value.isEmpty) {
      return 'Username is required.';
    }
    if (value.length < 3) {
      return 'Username must be at least 3 characters.';
    } else if (value.contains(' ') ||
        value.startsWith(' ') ||
        value.endsWith(' ')) {
      return 'Username must not contain whitespace.';
    } else {
      if (!RegExp(r'^(?=[a-zA-Z0-9._]{3,20}$)(?!.*[_.]{2})[^_.].*[^_.]$')
          .hasMatch(value)) {
        return 'Username must not contain special characters.';
      }
    }
    return null;
  }

  static String? validateName(String value) {
    if (value.isEmpty) {
      return 'Name is required.';
    }
    if (value.length < 3) {
      return 'Name must be at least 3 characters.';
    }
    if (!RegExp(r'^[a-zA-Z ]*$').hasMatch(value)) {
      return 'Name must not contain numbers or special characters.';
    }
    return null;
  }

  static String? validateConfirmPassword(
    String password,
    String confirmPassword,
  ) {
    if (confirmPassword.isEmpty) {
      return 'Confirm password is required.';
    }
    if (confirmPassword != password) {
      return 'Passwords do not match.';
    }
    return null;
  }

  static String? validateOtpCode(String value) {
    if (value.isEmpty) {
      return 'Code is required.';
    }
    if (value.length < 4) {
      return 'Invalid code.';
    }
    return null;
  }

  static String? validatePhoneNumber(String? phone) {
    if (phone == null || phone.isEmpty) {
      return 'Phone number is required.';
    }

    final RegExp phoneExp = RegExp(r'[0-9]{10}');
    if (!phoneExp.hasMatch(phone)) {
      return 'Please enter a valid phone number.';
    }
    return null;
  }

}
