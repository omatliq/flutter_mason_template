import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../main.dart';

final preferenceManagerProvider = Provider(
      (ref) => PreferenceManager(
    sharedPreferences: ref.read(sharedPreferencesProvider),
  ),
);

class PreferenceManager {
  const PreferenceManager({
    required SharedPreferences sharedPreferences,
  }) : _sharedPreferences = sharedPreferences;

  static const String isFirstTimeKey = 'is_first_time';
  static const String accessTokenKey = 'access_token';
  static const String userKey = 'user';
  static const String usernameKey = 'username';
  static const String passwordKey = 'password';
  static const String postCount = 'post_count';

  final SharedPreferences _sharedPreferences;

  String? getString(String key) {
    return _sharedPreferences.getString(key);
  }

  bool? getBool(String key) {
    return _sharedPreferences.getBool(key);
  }

  int? getInt(String key) {
    return _sharedPreferences.getInt(key);
  }

  setBool(String key, bool value) async {
    await _sharedPreferences.setBool(key, value);
  }

  setInt({required String key, required int value}) async {
    await _sharedPreferences.setInt(key, value);
  }

  setString({required String key, required String value}) async {
    await _sharedPreferences.setString(key, value);
  }

  removeItem(String key) async {
    await _sharedPreferences.remove(key);
  }
}